use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :dtu, DTUWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Make encryotion faster for dev purposes
config :bcrypt_elixir, :log_rounds, 4

# Configure your database
config :dtu, DTU.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "dtu_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
