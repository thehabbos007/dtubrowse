# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :dtu,
  namespace: DTU,
  ecto_repos: [DTU.Repo]

# Configures the endpoint
config :dtu, DTUWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "CB04r1pD/bENqnIV9fYMOGn7R8t3sOGkIT4VD/OqPYqA+LUXYOKaD63RLP1d5Mft",
  render_errors: [view: DTUWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: DTU.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
