defmodule DTU.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :password_hash, :string
      add :is_active, :boolean, default: false, null: false
      add :first_name, :string
      add :last_name, :string
      add :s_id, :string
      add :s_guid, :string

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
