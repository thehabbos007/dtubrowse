export interface LogoutStatus{
  status: number,
  data: string
}

export interface LoginUser{
  email: string,
  id: number
}