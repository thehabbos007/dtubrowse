export interface User {
  email: string,
  first_name: string,
  last_name: string,
  is_active: boolean,
  s_id: string,
  s_guid: string
}

export const emptyUser = {
  email: '',
  first_name: '',
  last_name: '',
  is_active: false,
  s_id: '',
  s_guid: ''
}

export interface ResUser{
  data: User
}

export interface DataFormat{
  success: boolean,
  data: string
}