import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { DashComponent } from './components/dash/dash.component'

import { LogoutComponent } from './components/logout/logout.component';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { AuthGuard} from './guards/auth.guard';

import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';

import { SidebarComponent } from './components/UI/sidebar/sidebar.component';
import { DashboardComponent } from './components/UI/dashboard/dashboard.component'
import { UserComponent }   from './components/UI/user/user.component';
import { UpgradeComponent }   from './components/UI/upgrade/upgrade.component';
import { TypographyComponent }   from './components/UI/typography/typography.component';
import { TableComponent }   from './components/UI/table/table.component';
import { NotificationsComponent }   from './components/UI/notifications/notifications.component';
import { IconsComponent }   from './components/UI/icons/icons.component';
//import { MapsComponent }   from './components/UI/maps/maps.component';

@NgModule({ 
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DashComponent,
    DashboardComponent,
    LogoutComponent,
    SidebarComponent,
    UserComponent,
    TableComponent,
    TypographyComponent,
    IconsComponent,
    //MapsComponent,
    NotificationsComponent,
    UpgradeComponent
  ],
  imports: [
    BrowserModule,
    NavbarModule,
    FooterModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [AuthService, UserService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
