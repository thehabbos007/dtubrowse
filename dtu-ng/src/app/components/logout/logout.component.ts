import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';

import { LogoutStatus } from '../../models/session'

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private user: UserService,
              private auth:AuthService,
              private router: Router) { }

  ngOnInit() {
    this.auth.logout().subscribe(
      (data:LogoutStatus) => { // success path, user is logged out
        this.auth.setloggedIn(false)
        this.router.navigate([''])
      }, 
      error => { // error path 
        //this.router.navigate([''])     
        this.router.navigate([''])
      } 
    )
  }

}

