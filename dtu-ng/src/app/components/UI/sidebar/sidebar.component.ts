import { Component, OnInit, OnDestroy } from '@angular/core';
import * as $ from 'jquery';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    loggedIn: boolean;
}

export const ROUTES: RouteInfo[] = [
    { path: 'login', title: 'Login',  icon:'ti-text', class: '', loggedIn: false},
    { path: 'dashboard', title: 'Dashboard',  icon: 'ti-panel', class: '', loggedIn: true},
    { path: 'user', title: 'User Profile',  icon:'ti-user', class: '', loggedIn: true},
    { path: 'table', title: 'Table List',  icon:'ti-view-list-alt', class: '', loggedIn: false},
    { path: 'icons', title: 'Icons',  icon:'ti-pencil-alt2', class: '', loggedIn: false},
    { path: 'maps', title: 'Maps',  icon:'ti-map', class: '', loggedIn: false},
    { path: 'notifications', title: 'Notifications',  icon:'ti-bell', class: '', loggedIn: false},
    { path: 'upgrade', title: 'Upgrade to PRO',  icon:'ti-export', class: 'active-pro', loggedIn: false},
];

@Component({
    selector: 'sidebar-cmp',
    templateUrl: './sidebar.component.html',
})

export class SidebarComponent implements OnInit, OnDestroy {
    public menuItems: any[];
    
    isLoggedIn: boolean;
    subscription: Subscription;


    constructor(private authService: AuthService){
        this.subscription = authService.isLoggedIn.subscribe(data =>{
            console.log(data);
            this.isLoggedIn = data
            this.ngOnInit()
        })
    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => {
            if(this.isLoggedIn){
                return menuItem.loggedIn
            }else{
                return !menuItem.loggedIn
            }
        });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe()
    }

    isNotMobileMenu(){
        if($(window).width() > 991){
            return false;
        }
        return false;
    }


}
