import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { Observable, Subscription } from 'rxjs';

@Component({
    selector: 'user-cmp',
    templateUrl: './user.component.html'
})

export class UserComponent implements OnInit, OnDestroy{

    user: User

    subscription: Subscription

    constructor(private userService : UserService) {
       // this.user = this.userService.getUser()
       this.subscription = userService.getUser().subscribe(data => {
        this.user = data
       })
    }

    ngOnInit(){
    }
    ngOnDestroy(): void {
        this.subscription.unsubscribe()
    }

    clicked(){
        console.log(this.user);
    }
}
