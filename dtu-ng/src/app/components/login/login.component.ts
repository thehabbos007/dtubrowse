import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

import { LoginUser } from '../../models/session';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService, 
              private user: UserService,
              private router: Router) { }

  ngOnInit() {
  }

  loginUser(event){
    event.preventDefault()
    const target = event.target
    const email = target.querySelector('#email').value
    const password = target.querySelector('#password').value

    this.auth.logIn(email, password).subscribe(
      (data:LoginUser) => { // success path
        this.user.setMe()
        this.auth.setloggedIn(true)
        this.router.navigate(['dash'])
      }, 
      error => { // error path 
        //this.router.navigate([''])     
          
      } 
    )
    console.log(email)
    console.log(password)
  }

}


