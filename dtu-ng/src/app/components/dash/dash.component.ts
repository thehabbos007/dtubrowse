import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent implements OnInit {
  message = "loading";

  constructor(private user: UserService) { }

  ngOnInit() {
    this.user.getSomeDate().subscribe(
      (data) => { // success path
        this.message = data.data
        console.log(data)
      }, 
      error => { // error path   
        this.message = error.statusText
        console.log(error)
      } 
    )
  }

}
