import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { AuthService } from './auth.service';

import {ResUser, User, DataFormat, emptyUser} from '../models/user';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  user = new BehaviorSubject<User>(emptyUser)
  //updateUser : User

  constructor(private http: HttpClient,
              private auth: AuthService) { }

  getSomeDate(){
    return this.http.get<DataFormat>('/api/some')
  }
  
  getUser(){
    return this.user.asObservable()
  }

  me() : Observable<ResUser>{
    return this.http.get<ResUser>('/api/me')
  }

//  setUpdate(updateUser:User){
//   this.updateUser = updateUser
//  }

  setMe(){
    this.me().subscribe(      
      (userData:ResUser) => { // success path
      this.user.next(userData.data)
    }, 
    error => {
      this.auth.setloggedIn(false)
    })
  }

  isLoggedIn() : Observable<isLoggedIn>{
    return this.http.get<isLoggedIn>('/api/users/logged_in')
  }

}

interface isLoggedIn{
  status: boolean
}