import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { LogoutStatus, LoginUser } from '../models/session';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private loggedInStatus = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) { }

  setloggedIn(value: boolean){
    console.log(value);
    
    this.loggedInStatus.next(value)
  }

  get isLoggedIn(){
    return this.loggedInStatus;
  }

  get isLoggedInObs(){
    return this.loggedInStatus.asObservable();
  }

  logIn(email: string, password: string) : Observable<LoginUser>{
    // post details to API, return user info plss
    return this.http.post<LoginUser>('/api/users/sign_in', {
      email,
      password
    }) 
  }

  logout(){
    return this.http.delete<LogoutStatus>("/api/users/sign_out")
  }

}