import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { DashComponent } from './components/dash/dash.component';
import { AuthGuard } from './guards/auth.guard';
import { LogoutComponent } from './components/logout/logout.component';

import { DashboardComponent } from './components/UI//dashboard/dashboard.component';
import { UserComponent }   from './components/UI/user/user.component';
import { UpgradeComponent }   from './components/UI/upgrade/upgrade.component';
import { TypographyComponent }   from './components/UI/typography/typography.component';
import { TableComponent }   from './components/UI/table/table.component';
import { NotificationsComponent }   from './components/UI/notifications/notifications.component';
import { IconsComponent }   from './components/UI/icons/icons.component';
import { IsLoggedInGuard } from './guards/is-logged-in.guard';
//import { MapsComponent }   from './components/UI/maps/maps.component';

const routes: Routes = [/*
  {path: 'login', component: LoginComponent},
  {path: 'dash', component: DashComponent, canActivate: [AuthGuard]},
  {path: 'logout', component: LogoutComponent, canActivate: [AuthGuard]},
  {path: '', component: HomeComponent},*/
  //{path: 'maps', component: MapsComponent},
  { path: '', redirectTo: 'dashboard', pathMatch: 'full', 
  canActivate: [IsLoggedInGuard]},
  
  { path: 'login', component: LoginComponent,
  canActivate: [IsLoggedInGuard] },
  
  { path: 'dashboard', component: DashboardComponent, 
  canActivate: [IsLoggedInGuard, AuthGuard] },
  
  { path: 'user', component: UserComponent,
  canActivate: [IsLoggedInGuard, AuthGuard] },
  
  { path: 'table', component: TableComponent,
  canActivate: [IsLoggedInGuard] },
  
  { path: 'icons', component: IconsComponent,
  canActivate: [IsLoggedInGuard] },
  
  { path: 'notifications', component: NotificationsComponent,
  canActivate: [IsLoggedInGuard] },
  
  { path: 'upgrade', component: UpgradeComponent,
  canActivate: [IsLoggedInGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
