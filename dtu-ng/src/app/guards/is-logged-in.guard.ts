import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedInGuard implements CanActivate {

  private loggedIn: boolean

  subscription: Subscription;

  constructor(private auth: AuthService, 
    private user: UserService){
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      this.subscription = this.auth.isLoggedIn.subscribe(data => {
        this.loggedIn = data
        if(this.loggedIn){
          this.subscription.unsubscribe()
          return true
        }
      })
   
      return this.user.isLoggedIn().pipe(map(res => {
        if(res.status){
          this.user.setMe();
          this.auth.setloggedIn(true)
          this.subscription.unsubscribe()
          return true
        }else{
          this.subscription.unsubscribe()
          return true
        }
      }))
      
  }
}
