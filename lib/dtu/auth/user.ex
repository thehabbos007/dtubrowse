defmodule DTU.Auth.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field(:password, :string, virtual: true)
    field(:password_hash, :string)
    field :first_name, :string
    field :is_active, :boolean, default: false
    field :last_name, :string
    field :s_guid, :string
    field :s_id, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password, :is_active, :first_name, :last_name, :s_id, :s_guid])
    |> validate_required([:email, :password, :is_active, :first_name, :last_name, :s_id, :s_guid])
    |> unique_constraint(:email)
    |> put_password_hash()
  end

  defp put_password_hash(
         %Ecto.Changeset{
           valid?: true,
           changes: %{password: password}
         } = changeset
       ) do
    change(changeset, password_hash: Bcrypt.hash_pwd_salt(password))
  end

  defp put_password_hash(changeset) do
    changeset
  end
end
