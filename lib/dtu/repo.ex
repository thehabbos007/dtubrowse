defmodule DTU.Repo do
  use Ecto.Repo,
    otp_app: :dtu,
    adapter: Ecto.Adapters.Postgres
end
