defmodule DTUWeb.Router do
  use DTUWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug(:fetch_session)
  end

  pipeline :api_auth do
    plug(:ensure_authenticated)
  end

  scope "/api", DTUWeb do
    pipe_through(:api)
    post("/users/sign_in", SessionController, :create)
    get("/users/logged_in", SessionController, :logged_in)
  end

  scope "/api", DTUWeb do
    pipe_through([:api, :api_auth])
    delete("/users/sign_out", SessionController, :delete)
    get("/me", SessionController, :me)
    get("/some", MockController, :some)

    resources("/users", UserController, except: [:new, :edit])
  end

  defp ensure_authenticated(conn, _opts) do
    current_user_id = get_session(conn, :current_user_id)

    if current_user_id do
      conn
    else
      conn
      |> put_status(:unauthorized)
      |> put_view(DTUWeb.ErrorView)
      |> render("401.json", message: "Unauthenticated user")
      |> halt()
    end
  end
end
