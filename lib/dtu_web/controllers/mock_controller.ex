defmodule DTUWeb.MockController do
  use DTUWeb, :controller

  action_fallback DTUWeb.FallbackController

  def some(conn, _params) do
    render(conn, "mock.json")
  end
end
