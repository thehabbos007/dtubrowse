defmodule DTUWeb.SessionController do
  use DTUWeb, :controller

  alias DTU.Auth
  alias DTU.Auth.User

  action_fallback DTUWeb.FallbackController

  # You know it
  def create(conn, %{"email" => email, "password" => password}) do
    case DTU.Auth.authenticate_user(email, password) do
      {:ok, user} ->
        conn
        |> put_session(:current_user_id, user.id)
        |> put_status(:ok)
        |> render("sign_in.json", user: user)

      {:error, message} ->
        conn
        |> delete_session(:current_user_id)
        |> put_status(:unauthorized)
        |> render(DTUWeb.ErrorView, "401.json", message: message)
    end
  end

  # You know it
  def delete(conn, _params) do
    conn
    |> clear_session
    |> render("sign_out.json")
  end

  def logged_in(conn, _params) do
    case get_session(conn, :current_user_id) do
      id -> render(conn, "logged_in.json", id: id)
      nil -> render(conn, "logged_in.json")
    end
  end

  def me(conn, _params) do
    user_id = get_session(conn, :current_user_id)
    user = Auth.get_user!(user_id)

    conn
    |> put_view(DTUWeb.UserView)
    |> render("show.json", user: user)
  end
end
