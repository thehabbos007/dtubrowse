defmodule DTUWeb.UserView do
  use DTUWeb, :view
  alias DTUWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      email: user.email,
      is_active: user.is_active,
      first_name: user.first_name,
      last_name: user.last_name,
      s_id: user.s_id,
      s_guid: user.s_guid
    }
  end
end
