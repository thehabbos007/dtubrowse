defmodule DTUWeb.SessionView do
  use DTUWeb, :view
  alias DTUWeb.SessionView

  def render("sign_in.json", %{user: user}) do
    %{
      data: %{
        user: %{
          id: user.id,
          email: user.email
        }
      }
    }
  end

  def render("sign_out.json", _assigns) do
    %{
      status: 200,
      data: %{
        message: "Signed out!"
      }
    }
  end

  def render("logged_in.json", %{id: id}) do
    %{status: true}
  end

  def render("logged_in.json", _params) do
    %{status: false}
  end
end
