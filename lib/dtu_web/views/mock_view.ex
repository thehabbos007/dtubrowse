defmodule DTUWeb.MockView do
  use DTUWeb, :view
  alias DTUWeb.SessionView

  def render("mock.json", _params) do
    %{
      success: true,
      data: "test"
    }
  end
end
